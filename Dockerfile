FROM ubuntu/nginx

COPY ./index.html var/www/html/index.nginx-debian.html
COPY ./primer.png var/www/html/primer.png

EXPOSE 80
